const prod = process.env.NODE_ENV === "production";
const webpack = require('webpack');

module.exports = {
    productionSourceMap:  !prod,
};
