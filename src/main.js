import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store/'

// ===================================== 外部套件 =====================================

//---- Bootstrap
import Bootstrap from 'bootstrap'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
//--- VEditor
import VEditor from './util/editor'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//---- MainCSS
import '../src/assets/css/style_admin.css'

//---- jQuery
import {jQuery} from 'jquery'

import VueClipboard from 'vue-clipboard2'
VueClipboard.config.autoSetContainer = true;

Vue.use(VueClipboard);

window.$ = jQuery;
window.JQuery = jQuery;

//---- ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI);

//---- echarts
import ECharts from 'vue-echarts'
// import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
// import 'echarts/lib/chart'
Vue.component('v-chart', ECharts);

import Public from './util/public'

Public.install = function (Vue) {
    Vue.prototype.$public = Public;
};
Vue.use(Public);


//---- Auth
import Auth from './util/auth'

Auth.install = function (Vue) {
    Vue.prototype.$auth = Auth;
};
Vue.use(Auth);

import Http from './util/http'

Http.install = function (Vue) {
    Vue.prototype.$http = Http;
};
Vue.use(Http);

Vue.config.productionTip = false

new Vue({
    VEditor,
    el: "#app",
    ECharts,
    Bootstrap,
    store,
    router,
    render: h => h(App)
}).$mount('#app')
