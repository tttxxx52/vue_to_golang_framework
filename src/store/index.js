import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';

//import app from "../main";

Vue.use(Vuex);
export const store = new Vuex.Store({
    strict: true,
    state,
    mutations: {
        updateCouponId(state, data) {
            state.coupon.id = data.id;
        },
        updateCouponNew1(state, data) {
            state.coupon.isDiscountShare = data.isDiscountShare;
            state.coupon.storeList = data.storeList;
            state.coupon.radioStore = data.radioStore;
            state.coupon.radioPublishDep = data.radioPublishDep;
        },
        updateCouponNew2(state, data) {
            state.coupon.name = data.name;
            state.coupon.content = data.content;
            state.coupon.contentPrecautions = data.contentPrecautions;
            state.coupon.radioDeadlineNotice = data.radioDeadlineNotice;
            state.coupon.logoImage = data.logoImage;
            state.coupon.logoPreview = data.logoPreview;
            state.coupon.contentImage = data.contentImage;
            state.coupon.contentPreview = data.contentPreview;
            state.coupon.startAt = data.startAt;
            state.coupon.endAt = data.endAt;
        },
        updateCouponNew3(state, data) {
            state.coupon.radioGetLimitType = data.radioGetLimitType;
            state.coupon.getPeriodLimit = data.getPeriodLimit;
            state.coupon.getDateLimit = data.getDateLimit;
            state.coupon.getDateLimitOwn = data.getDateLimitOwn;
            state.coupon.radioCouponType = data.radioCouponType;
            state.coupon.radioDiscountType = data.radioDiscountType;
            state.coupon.purchaseAmount = data.purchaseAmount;
            state.coupon.radioPublishEnable = data.radioPublishEnable;
            state.coupon.discountAmount = data.discountAmount;
            state.coupon.discountPercent = data.discountPercent;
            state.coupon.publishMethodCheckList = data.publishMethodCheckList;
            state.coupon.radioAmountPublishType = data.radioAmountPublishType;
            state.coupon.publishAmount = data.publishAmount;
            state.coupon.publishPieces = data.publishPieces;
            state.coupon.sendMax = data.sendMax;
            state.coupon.radioStatus = data.radioStatus;
            state.coupon.purchaseCode = data.purchaseCode;
            state.coupon.freebieCode = data.freebieCode;
            state.coupon.freebiePrice = data.freebiePrice;
        },
        updateCouponNew4(state, data) {
            state.coupon.radioConvertLimitType = data.radioConvertLimitType;
            state.coupon.convertDateLimit = data.convertDateLimit;
            state.coupon.convertPeriodLimit = data.convertPeriodLimit;
            state.coupon.radioUseSet = data.radioUseSet;
            state.coupon.radioGetLimitAmountType = data.radioGetLimitAmountType;
            state.coupon.getLimitOwn = data.getLimitOwn;
        },
        updateCouponNew5(state, data) {
            state.coupon.radioGiftType = data.radioGiftType;
            state.coupon.giftFrequencyLimit = data.giftFrequencyLimit;
            state.coupon.radioGiftLimitType = data.radioGiftLimitType;
            state.coupon.giftPeriodLimit = data.giftPeriodLimit;
            state.coupon.giftDayLimit = data.giftDayLimit;
            state.coupon.giftHourLimit = data.giftHourLimit;
            state.coupon.giftTypeCheckList = data.giftTypeCheckList;
        },
        clearCouponData(state) {
            state.coupon.id = 0;

            state.coupon.isDiscountShare = false;
            state.coupon.storeList = [];
            state.coupon.radioStore = '';
            state.coupon.radioPublishDep = '1';

            state.coupon.name = '';
            state.coupon.content = '';
            state.coupon.contentPrecautions = [''];
            state.coupon.radioDeadlineNotice = '2';
            state.coupon.logoImage = "";
            state.coupon.logoPreview = "";
            state.coupon.contentImage = "";
            state.coupon.contentPreview = "";

            state.coupon.radioGetLimitType = '0';
            state.coupon.getPeriodLimit = '1';
            state.coupon.getDateLimit = '1';
            state.coupon.getDateLimitOwn = '1';
            state.coupon.radioCouponType = '2';
            state.coupon.radioDiscountType = '2';
            state.coupon.purchaseAmount = '';
            state.coupon.radioPublishEnable = '2';
            state.coupon.startAt = '';
            state.coupon.endAt = '';
            state.coupon.discountAmount = '';
            state.coupon.discountPercent = '';
            state.coupon.publishMethodCheckList = [];
            state.coupon.radioAmountPublishType = '1';
            state.coupon.publishAmount = '';
            state.coupon.publishPieces = '';
            state.coupon.sendMax = '';
            state.coupon.radioStatus = '1';

            state.coupon.radioConvertLimitType = '1';
            state.coupon.convertDateLimit = '1';
            state.coupon.convertPeriodLimit = '';
            state.coupon.radioUseSet = '1';
            state.coupon.radioGetLimitAmountType = '1';
            state.coupon.getLimitOwn = '1';

            state.coupon.radioGiftType = '1';
            state.coupon.giftFrequencyLimit = '1';
            state.coupon.radioGiftLimitType = '0';
            state.coupon.giftPeriodLimit = '';
            state.coupon.giftDayLimit = '';
            state.coupon.giftHourLimit = '';
            state.coupon.giftTypeCheckList = [];
        },
        updateCouponSelect(state, data) {
            state.couponUser.couponData = data.couponData;
        },
        updateIssueNewPoints(state, data) {
            state.reward.action = data.action;
            state.reward.rewardPointListId = data.rewardPointListId;
        },
        updateIssueNewPoints1(state, data) {
            state.reward.pointName = data.pointName;
            state.reward.content = data.content;
            state.reward.contentPrecautions = data.contentPrecautions;
            state.reward.eventDate = data.eventDate;
            state.reward.btnExpiry = data.btnExpiry;
            state.reward.expiryDateKey = data.expiryDateKey;
            state.reward.expiryDateOptions = data.expiryDateOptions;
            state.reward.expiryDate = data.expiryDate;
            state.reward.logoImage = data.logoImage;
            state.reward.logoPreview = data.logoPreview;
            state.reward.startAt = data.startAt;
            state.reward.endAt = data.endAt;
            state.reward.upLoadImageFlag = data.upLoadImageFlag;
        },
        updateIssueNewPoints2(state, data) {
            // 1:開啟 2:關閉 - 發放狀態
            state.reward.btnIsIssue = data.btnIsIssue;
            state.reward.checkList = data.checkList;
            // 1:單筆 2:累積 - 金額發放規定
            state.reward.rewardPublishMoneyType = data.rewardPublishMoneyType;
            state.reward.customerPrice1 = data.customerPrice1;
            state.reward.customerPoint1 = data.customerPoint1;
            state.reward.customerPrice2 = data.customerPrice2;
            state.reward.customerPoint2 = data.customerPoint2;
            state.reward.maxAmount = data.maxAmount;
            // 1:無限制 2:點數限制 - 單筆最高發放限制
            state.reward.rewardPublishManualType = data.rewardPublishManualType;
            state.reward.rewardPublishManualIssueAmount = data.rewardPublishManualIssueAmount;
            // 1:依活動區間 2:依日期 - 領取限制
            state.reward.rewardPublishGetType = data.rewardPublishGetType;
            state.reward.rewardPublishGetTimes1 = data.rewardPublishGetTimes1;
            state.reward.rewardPublishGetTimes2 = data.rewardPublishGetTimes2;
            state.reward.btnIsShow = data.btnIsShow;
            state.reward.amount = data.amount;
        },
        clearIssueNewPoints(state) {
            state.reward.rewardPointListId = 0;
            state.reward.action = 'ins';
            state.reward.pointName = '';
            state.reward.content = '';
            state.reward.contentPrecautions = '';
            state.reward.btnExpiry = '1';
            state.reward.eventDate = '';
            state.reward.expiryDateKey = '';
            state.reward.expiryDateOptions = [];
            state.reward.logoImage = '';
            state.reward.logoPreview = '';
            state.reward.startAt = '';
            state.reward.endAt = '';
            state.reward.upLoadImageFlag = false;
            // 1:開啟 2:關閉 - 發放狀態
            state.reward.btnIsIssue = '1';
            state.reward.checkList = ['金額'];
            // 1:單筆 2:累積 - 金額發放規定
            state.reward.rewardPublishMoneyType = '1';
            state.reward.customerPrice1 = '';
            state.reward.customerPoint1 = '';
            state.reward.customerPrice2 = '';
            state.reward.customerPoint2 = '';
            state.reward.maxAmount = '';
            // 1:無限制 2:點數限制 - 單筆最高發放限制
            state.reward.rewardPublishManualType = '1';
            state.reward.rewardPublishManualIssueAmount = '';
            // 1:依活動區間 2:依日期 - 領取限制
            state.reward.rewardPublishGetType = '1';
            state.reward.rewardPublishGetTimes1 = '';
            state.reward.rewardPublishGetTimes2 = '';
            state.reward.btnIsShow = '1';
            state.reward.amount = '';
        },
        clearLoyaltyCardData(state) {
            state.loyaltyCard.radioGiftCountType= '1';
            state.loyaltyCard.id= 0;
            state.loyaltyCard.isActivateCardGiftPoint= '1';
            state.loyaltyCard.activateCardGiftPoint= 0;
            state.loyaltyCard.storeList= [];
            state.loyaltyCard.radioPublishDep= '1';
            state.loyaltyCard.radioStore= '';
            state.loyaltyCard.name= '';
            state.loyaltyCard.content= '';
            state.loyaltyCard.contentPrecautions= [''];
            state.loyaltyCard.radioStatus= '1';
            state.loyaltyCard.radioDeadlineNotice= '2';
            state.loyaltyCard.logoImage= "";
            state.loyaltyCard.logoPreview= "";
            state.loyaltyCard.contentImage= "";
            state.loyaltyCard.contentPreview= "";
            state.loyaltyCard.radioDiscountType= '2';
            state.loyaltyCard.radioPublishEnable= '2';
            state.loyaltyCard.startAt= '';
            state.loyaltyCard.endAt= '';
            state.loyaltyCard.publishMethodCheckList= [];
            state.loyaltyCard.radioAmountPublishType= '1';
            state.loyaltyCard.publishAmount= '';
            state.loyaltyCard.publishPoint= '';
            state.loyaltyCard.radioGetLimitType= '0';
            state.loyaltyCard.getPeriodLimit= '1';
            state.loyaltyCard.getDateLimit= '1';
            state.loyaltyCard.convertEndAt= "";
            state.loyaltyCard.convertStartAt= "";
            state.loyaltyCard.radioConvertEnable= '1';
            state.loyaltyCard.radioUseSet= '1';
            state.loyaltyCard.radioGiftType= '1';
            state.loyaltyCard.giftFrequencyLimit= '1';
            state.loyaltyCard.radioGiftLimitType= '0';
            state.loyaltyCard.giftPeriodLimit= '';
            state.loyaltyCard.giftDayLimit= '';
            state.loyaltyCard.giftHourLimit= '';
            state.loyaltyCard.giftTypeCheckList= [];
            state.loyaltyCard.convertType= [];
            state.loyaltyCard.selectedId= [];
            state.loyaltyCard.selectedName= [];
            state.loyaltyCard.couponList= [];
            state.loyaltyCard.consumePoint= [];
            state.loyaltyCard.convertAmount= [];
            state.loyaltyCard.purchasePrice= [];
            state.loyaltyCard.convertMaxRadio= [];
            state.loyaltyCard.convertMaxLimit=[];
            state.loyaltyCard.sendMaxLimit= [];
            state.loyaltyCard.purchaseConsumePoint= [];
            state.loyaltyCard.purchaseConvertAmount= [];
            state.loyaltyCard.remainNotice=[]
        },updateLoyaltyCardId(state, data) {
            state.loyaltyCard.id = data.id;
        },
        updateLoyaltyCardNew1(state, data) {
            state.coupon.isDiscountShare = data.isDiscountShare;
            state.coupon.storeList = data.storeList;
            state.coupon.radioStore = data.radioStore;
            state.coupon.radioPublishDep = data.radioPublishDep;
        },
        updateLoyaltyCardNew2(state, data) {
            state.loyaltyCard.name = data.name;
            state.loyaltyCard.content = data.content;
            state.loyaltyCard.contentPrecautions = data.contentPrecautions;
            state.loyaltyCard.radioDeadlineNotice = data.radioDeadlineNotice;
            state.loyaltyCard.logoImage = data.logoImage;
            state.loyaltyCard.logoPreview = data.logoPreview;
            state.loyaltyCard.contentImage = data.contentImage;
            state.loyaltyCard.contentPreview = data.contentPreview;
            state.loyaltyCard.startAt = data.startAt;
            state.loyaltyCard.endAt = data.endAt;
        },
        updateLoyaltyCardNew3(state, data) {
            state.loyaltyCard.isActivateCardGiftPoint = data.isActivateCardGiftPoint;
            state.loyaltyCard.activateCardGiftPoint = data.activateCardGiftPoint;
            state.loyaltyCard.radioGetLimitType = data.radioGetLimitType;
            state.loyaltyCard.getPeriodLimit = data.getPeriodLimit;
            state.loyaltyCard.getDateLimit = data.getDateLimit;
            state.loyaltyCard.radioPublishEnable = data.radioPublishEnable;
            state.loyaltyCard.radioAmountPublishType = data.radioAmountPublishType;
            state.loyaltyCard.publishMethodCheckList = data.publishMethodCheckList;
            state.loyaltyCard.publishAmount = data.publishAmount;
            state.loyaltyCard.publishPoint = data.publishPoint;
            state.loyaltyCard.radioStatus = data.radioStatus;
            state.loyaltyCard.publishGetAmount = data.publishGetAmount;

        },
        updateLoyaltyCardNew4(state, data) {
            state.loyaltyCard.convertStartAt = data.convertStartAt;
            state.loyaltyCard.convertEndAt = data.convertEndAt;
            state.loyaltyCard.radioConvertEnable = data.radioConvertEnable;
            state.loyaltyCard.radioUseSet = data.radioUseSet;
            store.state.loyaltyCard.convertType = data.convertType;
            store.state.loyaltyCard.selectedId = data.selectedId;
            store.state.loyaltyCard.selectedName = data.selectedName;
            store.state.loyaltyCard.couponList = data.couponList;
            store.state.loyaltyCard.consumePoint = data.consumePoint;
            store.state.loyaltyCard.convertAmount = data.convertAmount;
            store.state.loyaltyCard.purchasePrice = data.purchasePrice;
            state.loyaltyCard.purchaseConsumePoint= data.purchaseConsumePoint;
            state.loyaltyCard.purchaseConvertAmount= data.purchaseConvertAmount;
            store.state.loyaltyCard.convertMaxRadio = data.convertMaxRadio;
            state.loyaltyCard.sendMaxLimit = data.sendMaxLimit;
            state.loyaltyCard.remainNotice = data.remainNotice;
            state.loyaltyCard.convertMaxLimit = data.convertMaxLimit;
        },
        updateLoyaltyCardNew5(state, data) {
            state.loyaltyCard.radioGiftType = data.radioGiftType;
            state.loyaltyCard.giftFrequencyLimit = data.giftFrequencyLimit;
            state.loyaltyCard.radioGiftLimitType = data.radioGiftLimitType;
            state.loyaltyCard.giftPeriodLimit = data.giftPeriodLimit;
            state.loyaltyCard.giftDayLimit = data.giftDayLimit;
            state.loyaltyCard.giftHourLimit = data.giftHourLimit;
            state.loyaltyCard.giftTypeCheckList = data.giftTypeCheckList;
            state.loyaltyCard.radioGiftCountType = data.radioGiftCountType;
        },
        clearEventListData(state){
            state.eventList.id = 0;
            state.eventList.name = "";
            state.eventList.startAt = "";
            state.eventList.endAt = "";
            state.eventList.type = "";
            state.eventList.status = 0;
            state.eventList.createdAt = "";
            state.eventList.updatedAt = "";
            state.eventList.isJoin = 0;
            state.eventList.joinStoreList = "";
            state.eventList.description = "";
            state.eventList.isHighestStore = 0;
            state.eventList.storeLimit = "";
            state.eventList.isMaxCoupon = 0;
            state.eventList.couponLimit = "";
            state.eventList.projectDescription = "";
            state.eventList.price = "";
            state.eventList.applyStartAt = "";
            state.eventList.applyEndAt = "";
            state.eventList.applyStatus = 1;
            state.eventList.comment = "";
            state.eventList.eventListReviewId = 0;
            state.eventList.typeParamId = "";
            state.eventList.eventListReviewComment = "";
            state.eventList.eventListReviewCancelStatus = 0;
            state.eventList.eventListReviewCancelComment = "";
        },
        updateEventListData(state, data) {
            state.eventList.id = data.id;
            state.eventList.name = data.name;
            state.eventList.startAt = data.startAt;
            state.eventList.endAt = data.endAt;
            state.eventList.type = data.type;
            state.eventList.status = data.status;
            state.eventList.createdAt = data.createdAt;
            state.eventList.updatedAt = data.updatedAt;
            state.eventList.isJoin = data.isJoin;
            state.eventList.joinStoreList = data.joinStoreList;
            state.eventList.description = data.description;
            state.eventList.isHighestStore = data.isHighestStore;
            state.eventList.storeLimit = data.storeLimit;
            state.eventList.isMaxCoupon = data.isMaxCoupon;
            state.eventList.couponLimit = data.couponLimit;
            state.eventList.projectDescription = data.projectDescription;
            state.eventList.price = data.price;
            state.eventList.applyStartAt = data.applyStartAt;
            state.eventList.applyEndAt = data.applyEndAt;
            state.eventList.applyStatus = data.applyStatus;
            state.eventList.comment = data.comment;
            state.eventList.eventListReviewId = data.eventListReviewId;
            state.eventList.typeParamId = data.typeParamId;
            state.eventList.eventListReviewComment = data.eventListReviewComment;
            state.eventList.eventListReviewCancelStatus = data.eventListReviewCancelStatus;
            state.eventList.eventListReviewCancelComment = data.eventListReviewCancelComment;
        },
        clearBrandInfoData(state) {
            state.brandInfo.loginEventType = "一般",
            state.brandInfo.id = 0;
            state.brandInfo.account = "";
            state.brandInfo.password ="";
            state.brandInfo.brandName ="";
            state.brandInfo.storeTypeId = "";
            state.brandInfo.oinRoleId = "";
            state.brandInfo.professionId = "";
            state.brandInfo.logoPreview = "";
            state.brandInfo.logoImage = "";
            state.brandInfo.contractStartAt = "";
            state.brandInfo.contractEndAt = "";
            state.brandInfo.isEnable = true;
            state.brandInfo.contact = "";
            state.brandInfo.phone = "";
            state.brandInfo.email = "";
            state.brandInfo.address = "";
            state.brandInfo.companyTitle = "";
            state.brandInfo.taxIdNumber = "";
            state.brandInfo.note = "";
        },
        updateBrandInfoNew1(state, data) {

            state.brandInfo.brandName = data.brandName;
            state.brandInfo.storeTypeId = data.storeTypeId;
            state.brandInfo.oinRoleId = data.oinRoleId;
            state.brandInfo.professionId = data.professionId;
            state.brandInfo.logoPreview = data.logoPreview;
            state.brandInfo.logoImage = data.logoImage;
            state.brandInfo.contractStartAt = data.contractStartAt;
            state.brandInfo.contractEndAt = data.contractEndAt;
            state.brandInfo.isEnable = data.isEnable;
            state.brandInfo.account = data.account;
            state.brandInfo.password = data.password;
        },
        updateBrandInfoNew2(state, data) {
            state.brandInfo.contact = data.contact;
            state.brandInfo.phone = data.phone;
            state.brandInfo.email = data.email;
            state.brandInfo.address = data.address;
            state.brandInfo.companyTitle = data.companyTitle;
            state.brandInfo.taxIdNumber = data.taxIdNumber;
            state.brandInfo.note = data.note;
        },
        updateBrandInfoLoginType(state, data) {
            state.brandInfo.loginEventType = data.loginEventType;
        },
        updateBrandInfoToken(state, data) {
            state.brandInfo.token = data.token;
        },
    },
    actions: {}
})
