import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './views/MainPage.vue'
import Login from './views/Login.vue'
import Welcome from './views/Welcome'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: '',
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/admin',
            name: 'MainPage',
            component: MainPage,
            children: [
                ///////////////////////////////////////
                //               一般                //
                //////////////////////////////////////
                {path: 'welcome', name: 'Welcome', component: Welcome, meta: {title: 'Welcome'}},

            ]
        }
    ]
})
