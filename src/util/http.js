import crypto from "../util/crypto"
import auth from "../util/auth"
import Vue from 'vue'
import router from '../router'

export default {
    csrfToken: process.env.VUE_APP_CSRF_TOKEN,
    FileHost: process.env.VUE_APP_FILE_HOST,
    initCsrfToken() {
        let req = new XMLHttpRequest();
        req.open('GET', document.location, false);
        req.send(null);
        let headers = req.getAllResponseHeaders().split('\n');
        headers.forEach(function (item) {
            if (item.indexOf('x-csrf-token') !== -1) {
                this.csrfToken = item.substring(item.indexOf(":") + 1);
                return false;
            }
        });
    },
    fetchWithAuthEncrypt() {  /* 需要驗證的api都必須呼叫此方法 */
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const parameter = crypto.encryptText(JSON.stringify(args[1]));
        const funcSuccess = args[2];
        this.send(action, parameter, '/auth', funcSuccess);
    },
    fetchWithAuth() {  /* 需要驗證的api都必須呼叫此方法 */
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const parameter = JSON.stringify(args[1]);
        const funcSuccess = args[2];
        this.send(action, parameter, '/auth', funcSuccess);
    },
    fetchWithEncrypt() { /* 整個參數json 轉成 string 再進行加密 */
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const parameter = crypto.encryptText(JSON.stringify(args[1]));
        const funcSuccess = args[2];
        this.send(action, parameter, '', funcSuccess);
    },
    fetch() {
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const parameter = JSON.stringify(args[1]);
        const funcSuccess = args[2];
        this.send(action, parameter, '', funcSuccess);
    },
    async send(action, parameter, authPath, funcSuccess) {
        await fetch(process.env.VUE_APP_API_HOST + authPath, {
            // await fetch("http://192.168.1.130:9120" + "/admin-api" + authPath, {
            body: JSON.stringify({
                'action': action,
                'parameters': parameter,
            }),
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Csrf-Token': this.csrfToken === '' || typeof csrfToken === 'undefined' ? process.env.VUE_APP_CSRF_TOKEN : this.csrfToken,
                'Auth-Token': authPath === '/auth' ? auth.getToken() : '',
            }),
            method: 'PosT'
        }).then(response => {
            return response.text();
        }).then(text => {
            // JSON Hijacking while(1);
            let json = JSON.parse(text.replace('while(1);', ''));
            if (!json.status && json.message === "logout") {
                Vue.prototype.$auth.clearToken();
                router.push({path: '/'});
                return;
            }
            funcSuccess(json);
        });
    },

    /*
 ex: array file
    let imageAry = [];
    imageAry.push(this.logoImage);
    imageAry.push(this.logoImage);
    //按下確定後在＋
    this.$http.fetchUpload`${{
        "file":  imageAry,
        "folder":  "/test"
    }}
                    ${json => {
        console.log(json)
    }}`;

 ex: only file
     this.logoImage = file;
    //按下確定後在＋
    this.$http.fetchUpload`${{
        "file":   this.logoImage,
        "folder":  "/test"
    }}
    ${json => {
        console.log(json)
    }}`;
    */
    async fetchUpload() {
        let self = this;
        const args = Array.prototype.slice.call(arguments);
        const parameter = args[1];
        const funcSuccess = args[2];
        const funcFail = args[3];

        await fetch(process.env.VUE_APP_FILE_HOST + "/token-precede", {
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
            }),
            method: 'POST',
        }).then(response => {
            return response.text();
        }).then(text => {
            let tokenPrecede = JSON.parse(text.replace('while(1);', ''));
            self.uploadSend(parameter.file, parameter.folder, funcSuccess, funcFail, tokenPrecede, 0);
        }).catch(err => {
            if (funcFail) {
                funcFail(err);
            }
        });
    },
    async fetchUploadWithBase() {
        let self = this;
        const args = Array.prototype.slice.call(arguments);
        const parameter = args[1];
        const funcSuccess = args[2];
        const funcFail = args[3];

        await fetch(process.env.VUE_APP_FILE_HOST + "/token-precede", {
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
            }),
            method: 'POST',
        }).then(response => {
            return response.text();
        }).then(text => {

            let tokenPrecede = JSON.parse(text.replace('while(1);', ''));
            self.uploadSend(parameter.file, parameter.folder, funcSuccess, funcFail, tokenPrecede, 1);
        }).catch(err => {
            if (funcFail) {
                funcFail(err);
            }
        });
    },
    async uploadSend(file, folder, funcSuccess, funcFail, tokenPrecede, flag) {
        let data = new FormData();
        if (file[0] === undefined) {
            data.append("file", file);
        } else {
            file.forEach(function (val) {
                data.append("file", val);
            })
        }

        data.append("folder", folder);
        data.append("apiKey", "77bb1019afab95d2398de0b21fb9f399");
        let apiAction = (flag === 0) ? "/file-server-api" : "/file-server-base-api"
        await fetch(process.env.VUE_APP_FILE_HOST + apiAction, {
            body: data,
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Auth-Token': crypto.fileEncryptText("77bb1019afab95d2398de0b21fb9f399" + ";" + tokenPrecede)
            }),
            method: 'POST',

        }).then(response => {
            return response.text();
        }).then(text => {
            // JSON Hijacking while(1);
            let json = JSON.parse(text.replace('while(1);', ''));
            funcSuccess(json);
        }).catch(err => {
            console.log(err)
            if (funcFail) {
                funcFail(err);
            }
        });
    },

}







