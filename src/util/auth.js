/* eslint-disable semi,no-trailing-spaces,indent,quotes,space-infix-ops,comma-dangle,padded-blocks,no-unused-vars,eol-last,semi-spacing */


let admin_token = window.localStorage.getItem('vue_user_token');


export default {
    isUserLogin() {
        return admin_token !== null && admin_token !== '';
    },
    setUserToken(t) {
        window.localStorage.setItem('vue_user_token', t + '');
        admin_token = t;
    },
    getUserToken() {
        return admin_token;
    },
    clearToken() {
        admin_token = '';
        window.localStorage.removeItem('vue_user_token');
    },
}
